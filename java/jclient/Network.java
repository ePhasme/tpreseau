package jclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Network {
	
	private DataInputStream dis;
	private DataOutputStream dos;
	private Socket sock;
	
	void send(String str) {
		try {
			dos.writeBytes(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void send(char c) {
		try {
			dos.writeChar(c);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void send(int num) {
		send(String.valueOf(num));
	}
	
	String fetchString() {
		try {
			int c;
			StringBuilder build = new StringBuilder();
			while ((c = dis.read()) != 0) {
				build.append((char)c);
			}
			System.out.println(build.toString());
			return build.toString();
		} catch (IOException e) {
			throw new RuntimeException("fetch string error", e);
		}
	}
	
	char fetchChar() {
		String str = fetchString();
		if (str.length() > 0) {
			return str.charAt(0);
		}
		throw new RuntimeException("null str received");
	}
	
	int fetchNum() {
		String str = fetchString();
		return Integer.parseInt(str);
	}
	
	public Network(String host, int port) {
		try {
			sock = new Socket(host, port);
			dis = new DataInputStream(sock.getInputStream());
			dos = new DataOutputStream(sock.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		if (sock != null) {
			sock.close();
		}
	}
	
}
