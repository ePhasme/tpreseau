package jclient;
import java.util.Scanner;

public class Client
{
	private static final int POKER = 4;
	static Scanner scan = new Scanner(System.in);
	private static int CARD_VALUE_MIN = (int)'A';
	private static int CARD_VALUE_MAX = (int)'M';
	private static int HAND_SIZE = 5;
	
    
    static void cli_hidrate_card_list(char[] hand, char[] choices, char[] n) {
        int i = 0;
        int choice = 0;



        while (choice != 5 && i < 3) {

            System.out.printf("Choix d'une carte 5 pour arrêter)\n ");
				choice=scan.nextInt();
				System.out.println("choix :" +choice);
            
            if (choice != 5) {
                if (choice >= 0 && choice < 5) {
                    choices[i] = (char) choice;
                    System.out.println("Choix" + choice+ " enregistré.");
                    i++;
                } else {
                	System.out.printf("Mauvaise entrée.\n");
                    i--;
                }
            }
        }

        n[0] = (char) i;
        System.out.printf("Arrêt des choix (%d carte(s) ont été choisie(s)).\n", i);
        
    }
    
    private static int get_best_combination(char[] hand) {
        int primes[]={2,3,5,7,11,13,17,23,31,37,41,43,47};
        char cours;
        int min;
        int i,imin=hand[0]-CARD_VALUE_MIN;
        int sim, simrec = 0;
        int retour;
        int suite;
        long produit = 1, produitCopy;
        min = primes[hand[0]-CARD_VALUE_MIN];
        for (i = 0; i < HAND_SIZE; ++i) {
            produit *= primes[hand[i]-CARD_VALUE_MIN];
            if (primes[hand[i]-CARD_VALUE_MIN] < min)
            {
                min=primes[hand[i]-CARD_VALUE_MIN];
                imin=hand[i]-CARD_VALUE_MIN;
            }
            
        }
        produitCopy = produit;
        for (cours = (char)CARD_VALUE_MIN; cours <(char)( CARD_VALUE_MAX+1); ++cours) {
            sim = 0;
            while (produit % primes[cours-CARD_VALUE_MIN] == 0) {
                produit /= primes[cours-CARD_VALUE_MIN];
                ++sim;
            }
            simrec = simrec < sim ? sim : simrec;
        }
        --simrec;
        if (simrec == 4)
            --simrec;
        suite=min;
        for (i=1;i<5;i++)
            suite *= primes[imin+i];
                
        suite=suite==produitCopy?POKER:0;
                    
        retour = (suite > simrec) ? suite : simrec;

        return retour;
    }
    
    static void cli_display_hand(char[] hand) {
        int i;
        System.out.printf("Vos cartes sont :\n");
        for (i = 0; i < 5; i++) 
        	System.out.printf("\t%d) %c\n", i, hand[i]);
    }
    
  
    
    public static void main(String[] args) throws Exception {
		char[] hand = new char[6];
		char[] choices = new char[3];

		char[] n = new char[1];
		
		String login = "";
		String adresse = "localhost";
		int port = 7777;

		if (args.length > 0)
			adresse = args[0];
		if (args.length > 1)
			port = Integer.parseInt(args[1]);
		
		Network network = new Network(adresse, port);
		System.out.println("Client en écoute sur "+adresse+":"+port);
		
		System.out.println("Saisissez votre identifiant");
		login = scan.nextLine();

		network.send(login);
		
		do {
			for (int i = 0; i < 5; ++i)
				hand[i] = network.fetchChar();

			cli_display_hand(hand);

			cli_hidrate_card_list(hand, choices, n);
			network.send(((int) n[0]));

			for (int i = 0; i < ((int) n[0]); i++)
				hand[choices[i]] = network.fetchChar();
			cli_display_hand(hand);
			int j = 0;
			
			j = get_best_combination(hand);
			network.send(1);
			
			String resultat = null;
			switch (j) {
			case 0:
				resultat = "rien";
				break;
			case 1:
				resultat = "une paire";
				break;
			case 2:
				resultat = "un brelan";
				break;
			case 3:
				resultat = "un carre";
				break;
			case 4:
				resultat = "un poker";
				break;
			}
			
			System.out.println("Vous avez " + resultat);

			System.out.println("Joueur " + network.fetchNum() + " a gagné");
			
			System.out.println("Voulez-vous rejouer? (o/n)");
			String s = scan.next();
			network.send(s.equals("o") ? 1 : 0);

		} while (network.fetchNum() == 1);
		
		System.out.println("Fin de partie.");

    }
}