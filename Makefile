DEBUG = -DDEBUG -g

CC = gcc
JC = javac
CFLAGS = -W -Wall -ansi -pedantic
CFLAGS += $(DEBUG)
LDFLAGS = -g

C_BIN=client server
JAVA_BIN=jclient

BIN_PATH = bin
OBJ_PATH = obj
SRC_PATH = src
JAVA_SRC_PATH = java/jclient
JAVA_CLASS_PATH = bin

OBJS = $(OBJ_PATH)/network.o

all: $(C_BIN) $(JAVA_BIN)

## APPLICATIONS

$(C_BIN): %: $(OBJS) $(OBJ_PATH)/%.o $(OBJ_PATH)/main_%.o
	$(CC) $^ -o $(BIN_PATH)/$@ $(CFLAGS)

tests: $(OBJ_PATH)/client.o $(OBJ_PATH)/network.o $(OBJ_PATH)/tests.o
	$(CC) $^ -o $(BIN_PATH)/$@ $(CFLAGS)

jclient:
	$(JC) $(JAVA_SRC_PATH)/* -d $(JAVA_CLASS_PATH)/

## GENERATION DES OBJETS

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(LDFLAGS) -c $< -o $@
	
## CLEANUP

.PHONY=clean
clean:
	rm -rf *~
	rm -rf *.o
	rm -rf $(OBJ_PATH)/*
	
.PHONY=mrproper
mrproper: clean
	rm -rf $(BIN_PATH)/*