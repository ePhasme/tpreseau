#include "network.h"

iterator_body(int);

void net_fetch_string(int socket, char* login, int size) {
    recv(socket, login, size, 0);
    login[size - 1] = 0;
}

char net_fetch_char(int socket) {
    char c[2];
    recv(socket, c, 2, 0);
    return c[0];
}
int net_fetch_num(int socket) {
    char buf[16];
    net_fetch_string(socket, buf, 16);
    return atoi(buf);
}
int net_send_char(int socket, char c) {
    char buf[2] = { c, '\0' };
    return net_send_str(socket, buf);
}
int net_send_num(int socket, int c) {
    char buf[16] = {0};
    sprintf(buf, "%d", c);
    return net_send_str(socket, buf);
}
int net_send_str(int socket, char* string)  {
    return send(socket, string, strlen(string) + 1, 0);
}


void net_push_client(network_t* this, int client_socket) {
   if (this->size < NET_MAX_CLIENT_NB) {
        this->clients[this->size] = client_socket;
        this->size++;
    } else {
        fprintf(stderr, "NETWORK: Error, client booted, network full.\n");
    }
}

void net_stop(network_t* this) {
    int i;
    for (i = 0; i < this->size; i++) {
        close(this->clients[i]);
    }
    this->size = 0;
    close(this->socket);
}

void net_start(network_t* this, int port) {
    
    struct sockaddr_in addr;
    
    memset(this, NET_INVALID_SOCKET, sizeof (network_t));
    this->size = 0;

    this->socket = socket(AF_INET, SOCK_STREAM, 0);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(this->socket, (struct sockaddr *) &addr, sizeof (struct sockaddr_in)) != 0) {
        fprintf(stderr, "NETWORK: Error, bind impossible.\n");
        exit(EXIT_FAILURE);
    }

    if (listen(this->socket, 5) != 0) {
        fprintf(stderr, "NETWORK: Error, listening impossible.\n");
        exit(EXIT_FAILURE);
    }

}

int net_connect(int port, char* hostname) {
    struct sockaddr_in addr;
    struct hostent* entry;
    int sock;

    addr.sin_port = htons(port);
    addr.sin_family = AF_INET;

    entry = (struct hostent *) gethostbyname(hostname);
    bcopy((char *) entry->h_addr, (char *) &addr.sin_addr, entry->h_length);

    sock = socket(AF_INET, SOCK_STREAM, 0);

    if (connect(sock, (struct sockaddr *) &addr, sizeof (struct sockaddr_in)) < 0) {
        fprintf(stderr, "NETWORK: Error, impossible to reach the server.\n");
        exit(EXIT_FAILURE);
    }

    return sock;
}
