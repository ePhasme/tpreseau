#include "server.h"

int main(int argc, char** argv) {
    
    server_t server;
    int port = NET_DEFAULT_PORT;
    int cli_nb = DEFAULT_PLAYER_NB;
    
    if (argc > 1) {
        port = atoi(argv[1]);
    }
    if (argc > 2) {
        cli_nb = atoi(argv[2]);
    }
    
    serv_start(&server, port, cli_nb);
    
    return (EXIT_SUCCESS);
}

