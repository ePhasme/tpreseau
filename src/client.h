#ifndef CLIENT_H_
#define CLIENT_H_

#include "network.h"
#include "game_data.h"

int cli_get_best_combination(char* hand);
void cli_hidrate_card_list(char* hand, char* choices, char* n);
void cli_fetch_hand(char* hand, int socket);
void cli_display_hand(char* hand);
void cli_hidrate_login(char* login);

#endif // CLIENT_H_