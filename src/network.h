#ifndef NETWORK_H
#define	NETWORK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>

#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>

#include "lib.h"

#define NET_INVALID_SOCKET                      -1
#define NET_MAX_CLIENT_NB                           1023
#define NET_MSG_SIZE                            1023
#define NET_DEFAULT_PORT                        7777

typedef struct network {
    int socket;
    int size;
    int clients[NET_MAX_CLIENT_NB];
} network_t;

iterator_header(int);

int net_send_str(int socket, char* string);
int net_send_char(int socket, char c);
int net_send_num(int socket, int c);
void net_fetch_string(int socket, char* login, int size);
char net_fetch_char(int socket);
int net_fetch_num(int socket);
void net_push_client(network_t* this, int client_socket);
void net_stop(network_t* this);
void net_start(network_t* this, int port);
int net_connect(int port, char* hostname);

#endif	/* NETWORK_H */
