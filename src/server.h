#ifndef SERVER_H_
#define SERVER_H_

#include "lib.h"
#include "game_data.h"
#include "network.h"

typedef struct player {
    char login[LOGIN_LENGTH];
} player_t;

typedef struct server {
    char running;
    network_t network;
    player_t players[NET_MAX_CLIENT_NB];
    int nb_players;
} server_t;

iterator_header(player_t);

void serv_game_loop(server_t* this);
char serv_get_random_card();
void serv_fetch_clients(server_t* this);
void serv_fetch_logins(server_t* this);
void serv_fetch_card_changes(server_t* this);
void serv_send_hand(server_t* this);
void serv_start(server_t* this, int port, int nb_clients);

#endif // SERVER_H_
