#include "server.h"

iterator_body(player_t)

void serv_game_loop(server_t* this) {
    player_t* player = NULL;
    int i;
    int player_id = 0;
    int card_id = 0;
    int change_nb = 0;
    int winner = 0;
    int best_score = 0;
    
    printf("SERVER: New game loop.\n");
    
    player_t_iterator(this->players, this->nb_players);
    for (player_id = 0; has_next(player_t, player); player_id++) {

        int player_socket = this->network.clients[player_id];
        
        printf("SERVER: Traitement du joueur %d.\n", player_id);
        
        printf("SERVER: Envoi des cartes.\n");
        for (card_id = 0 ; card_id < HAND_SIZE ; card_id++) {
            net_send_char(player_socket, serv_get_random_card());
        }
        
        change_nb = net_fetch_num(player_socket);
        printf("SERVER: Le joueur veut changer %d cartes.\n", change_nb);
        for (i = 0 ; i < change_nb ; i++) {
            net_send_char(player_socket, serv_get_random_card());
        }
        
        printf("SERVER: Attente de la combinaison.\n");
        winner = (net_fetch_num(player_socket) > best_score) ? player_id : winner;
        printf("SERVER: Gagnant est %d\n", winner);
    }
    
    player_t_iterator(this->players, this->nb_players);
    for (player_id = 0; has_next(player_t, player); player_id++) {
        int player_socket = this->network.clients[player_id];
        net_send_num(player_socket, winner);
        
        // on vérifie que tout le monde veut continuer.
        this->running &= net_fetch_num(player_socket);
    }

    
    // On prévient les joueurs si le serveur est inactif.
    player_t_iterator(this->players, this->nb_players);
    for (player_id = 0; has_next(player_t, player); player_id++) {
        int player_socket = this->network.clients[player_id];
        net_send_num(player_socket, this->running);
    }    
    
    sleep(1);
}

char serv_get_random_card() {
    return (rand() % CARD_VALUE_NB) + CARD_VALUE_MIN;
}

void serv_fetch_clients(server_t* this) {
    int i;
    network_t* network = &(this->network);
    struct sockaddr_storage cli_recep;
    socklen_t cli_socklen;
    for (i = 0; i < this->nb_players && i < NET_MAX_CLIENT_NB; i++) {
        printf("SERVER: Attente de la connection du client %d.\n", i);
        net_push_client(network, accept(network->socket, (struct sockaddr*) &cli_recep, &cli_socklen));
    }
}

void serv_fetch_logins(server_t* this) {
    player_t* player = NULL;
    int player_id = 0;
    player_t_iterator(this->players, this->nb_players);
    for (player_id = 0; has_next(player_t, player); player_id++) {
        printf("SERVER: Attente du login du joueur %d.\n", player_id);
        net_fetch_string(this->network.clients[player_id], player->login, LOGIN_LENGTH);
        printf("SERVER: Login du joueur %d is '%s'.\n", player_id, player->login);
    }
}

void serv_start(server_t* this, int port, int nb_clients) {
    memset(this, 0, sizeof(server_t));

    net_start(&(this->network), port);
    
    this->running = true;
    this->nb_players = nb_clients;
    
    serv_fetch_clients(this);
    serv_fetch_logins(this);
    
    while (this->running) { serv_game_loop(this); }
    
    net_stop(&(this->network));
    
    printf("SERVER: Server session finished.\n");
}
