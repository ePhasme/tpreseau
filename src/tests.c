#include <stdio.h>
#include <stdlib.h>

#include "client.h"

#define assert_print(__result)  printf("=> "#__result" \t '%s' should %s\n", unit_name, test_name);

#define assertFalse(__expr) __res = (0) ? __res : 0
#define assertTrue(__expr) __res = ((__expr) != 0) ? __res : 0
#define assertEquals(__v, __exp) __res = (__v == __exp) ? __res : 0
#define assertEmpty_str(__str) __res = (strcmp(__str, "") == 0)  ? __res : 0
#define assertEquals_str(__str, __exp) __res = (strcmp(__str, __exp) == 0)  ? __res : 0
#define assertEquals_mem(__v, __mexp, __size) __res = (memcmp(__v, __mexp, __size) == 0)  ? __res : 0

#define function(__name) void __name##_tests() { char unit_name[256] = #__name;
#define should(__test) { char test_name[65635] = __test; int __res = 1; test_nb++;
#define and if (__res) { test_valid++; assert_print(SUCCESS); } else { test_invalid++; assert_print(FAILURE); } }
#define nothing_else }

int test_nb = 0;
int test_valid = 0;
int test_invalid = 0;

function(cli_get_best_combination)
    
    should("return NOTHING when given no pattern")
        char hand[5] = {'M','D','C','F','B'};
        int exp = NOTHING;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);

    and should("return PAIR when given basic pair")
        char hand[5] = {'D','D','C','G','B'};
        int exp = PAIR;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);

    and should("return PAIR when given mixed pair")
        char hand[5] = {'D','C','C','D','B'};
        int exp = PAIR;
        int res = -12;
        
        res = cli_get_best_combination(hand);

        assertEquals(exp, res);
        
    and should("return BRELAN when given basic brelan")
        char hand[5] = {'D','D','D','G','B'};
        int exp = BRELAN;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);
    
    and should("return BRELAN when given mixed brelan")
        char hand[5] = {'D','D','G','D','B'};
        int exp = BRELAN;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);    
    
    and should("return BRELAN when given brelan and pair")
        char hand[5] = {'D','D','G','D','G'};
        int exp = BRELAN;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);       
    
    and should("return CARRE when given basic carre")
        char hand[5] = {'D','D','D','D','B'};
        int exp = CARRE;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);
    
    and should("return CARRE when given mixed carre")
        char hand[5] = {'D','D','B','D','D'};
        int exp = CARRE;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);    
    
    and should("return CARRE when given five identical cards")
        char hand[5] = {'D','D','D','D','D'};
        int exp = CARRE;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);    
    
    and should("return POKER when given straight poker")
        char hand[5] = {'A','B','C','D','E'};
        int exp = POKER;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);
    
    and should("return POKER when given mixed poker")
        char hand[5] = {'A','C','D','E','B'};
        int exp = POKER;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);    
    and should("work when given a combination that produce a high product")

        char hand[5] = {'M','M','M','M','M'};
        int exp = CARRE;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);
    and should("??? WTF")
    
        char hand[5] = {'B', 'J', 'M', 'E', 'B'};
        int exp = PAIR;
        int res = -12;
        
        res = cli_get_best_combination(hand);
        
        assertEquals(exp, res);    
    and nothing_else

int main() {
    cli_get_best_combination_tests();
    return (EXIT_SUCCESS);
}
