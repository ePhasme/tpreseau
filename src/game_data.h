#ifndef GAME_DATA_H
#define	GAME_DATA_H

#define DEFAULT_PLAYER_NB 4
#define LOGIN_LENGTH    25
#define HAND_SIZE       5
#define MAX_CHOICES_NUMBER 3

#define CARD_VALUE_MIN          'A'
#define CARD_VALUE_MAX          'M'
#define CARD_VALUE_NB          (CARD_VALUE_MAX + 1 - CARD_VALUE_MIN)

#define NOTHING 0
#define PAIR    1
#define BRELAN    2
#define CARRE    3
#define POKER    4

#endif	/* GAME_DATA_H */
