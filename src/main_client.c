#include "client.h"

int main(int argc, char** argv) {

    srand(48);

    int i;
    char cont;
    int port = NET_DEFAULT_PORT;
    char choices[HAND_SIZE] = {0};
    char choices_nb = 0;
    int winner = 0;
    char login[LOGIN_LENGTH] = {0};
    int server = -1;
    char hand[HAND_SIZE] = {0};
    char running = true;
    char addr[25] = {0};


    if (argc > 1) {
        strcpy(addr, argv[1]);
        printf("%s",addr);
    }
    if (argc > 2) {
        port = atoi(argv[2]);
        printf("%d", port);
    }
    // Try to reach the server.
    server = net_connect(port, addr);
    printf("Client started on port %s:%d\n", addr, port);

    // Define my login.
    cli_hidrate_login(login);
    net_send_str(server, login);

    while (running) {

        // Fetch my hand.
        cli_fetch_hand(hand, server);

        // On récupère le nombre de cartes à changer.
        cli_hidrate_card_list(hand, choices, &choices_nb);
        net_send_num(server, choices_nb);
        printf("Nombre de carte à changer : %d\n", choices_nb);

        // On récupère les cartes.
        for (i = 0 ; i < choices_nb ; i++) {
            char c = net_fetch_char(server);
            printf("Carte %c (%d) changée pour %c\n", hand[choices[i]], choices[i], c);
            hand[choices[i]] = c;
        }

        // On montre la main.
        cli_display_hand(hand);
		printf("test\n");
        int comb = cli_get_best_combination(hand);

        char resultat[25] = "";
        switch (comb) {
            case 0:
                strcpy(resultat, "rien");
                break;
            case 1:
                strcpy(resultat, "une paire");
                break;
            case 2:
                strcpy(resultat, "un brelan");
                break;
            case 3:
                strcpy(resultat, "un carre");
                break;
            case 4:
                strcpy(resultat, "un poker");
                break;
        }
        
        printf("Votre meilleure combinaison est %s\n", resultat);

        // On envoie la combinaison au serveur.
        net_send_num(server, comb);

        // On reçoit le gagnant.
        winner = net_fetch_num(server);
        printf("Le gagnant est le joueur %d !\n", winner);

        printf("Voulez-vous continuer à jouer ? (o/n)\n");
        ioflush
        scanf("%c", &cont);

        net_send_num(server, cont == 'o' ? 1 : 0);

        // On vérifie que le serveur est vivant.
        running = net_fetch_num(server);

    }
    printf("La partie est finie.\n");

    return (EXIT_SUCCESS);
}
