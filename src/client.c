#include "client.h"
#include "server.h"

int cli_get_best_combination(char* hand) {
    int primes[13]={2,3,5,7,11,13,17,23,31,37,41,43,47};
    char cours;
    int min;
    int i,imin=hand[0]-CARD_VALUE_MIN;
    int sim, simrec = 0;
    int retour;
    int suite;
    uint produit = 1, produitCopy;
    min = primes[hand[0]-CARD_VALUE_MIN];
    for (i = 0; i < HAND_SIZE; ++i) {
        produit *= primes[hand[i]-CARD_VALUE_MIN];
        if (primes[hand[i]-CARD_VALUE_MIN] < min)
        {
            min=primes[hand[i]-CARD_VALUE_MIN];
            imin=hand[i]-CARD_VALUE_MIN;
        }
        
    }
    produitCopy = produit;
    for (cours = CARD_VALUE_MIN; cours < CARD_VALUE_MAX+1; ++cours) {
        sim = 0;
        while (produit % primes[cours-CARD_VALUE_MIN] == 0) {
            produit /= primes[cours-CARD_VALUE_MIN];
            ++sim;
        }
        simrec = simrec < sim ? sim : simrec;
    }
    --simrec;
    if (simrec == 4)
        --simrec;
    suite=min;
    for (i=1;i<5;i++)
        suite *= primes[imin+i];
            
    suite=suite==produitCopy?POKER:0;
                
    retour = (suite > simrec) ? suite : simrec;

    return retour;
}

void cli_hidrate_card_list(char* hand, char* choices, char* n) {
    int i = 0;
    int choice = 0;

    cli_display_hand(hand);

    while (choice != HAND_SIZE && i < MAX_CHOICES_NUMBER) {
        ioflush
        printf("Choix d'une carte (%d pour arrêter) :\n", HAND_SIZE, HAND_SIZE);
        scanf("%d", &choice);
        if (choice != HAND_SIZE) {
            if (choice >= 0 && choice < HAND_SIZE) {
                choices[i] = choice;
                printf("Choix %d enregistré.\n", choice);
                i++;
            } else {
                printf("Mauvaise entrée.\n");
                i--;
            }
        }
    }

    *n = i;
    printf("Arrêt des choix (%d carte(s) ont été choisie(s)).\n", i);
}

void cli_fetch_hand(char* hand, int socket) {
    int i;
    for (i = 0 ; i < HAND_SIZE ; i++) {
        hand[i] = net_fetch_char(socket);
    }
}

void cli_display_hand(char* hand) {
    int i;
    printf("Vos cartes sont :\n");
    for (i = 0; i < HAND_SIZE; i++) {
        printf("\t%d) %c\n", i, hand[i]);
    }
}

void cli_hidrate_login(char* login) {
    printf("Veuillez entre votre login : ");
    scanf("%s", login);
}
