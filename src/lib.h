#ifndef LIB__H
#define	LIB__H

#define true 1
#define false 0

#define ioflush {char ch; while ((ch = getchar()) != '\n' && ch != EOF);}

#define has_next(__type, __var_name) ((__var_name = __type##_iterator(NULL,-1)) != NULL)
#define iterator_header(__type) __type* __type##_iterator(__type* head, int size)
#define iterator_body(__type) \
iterator_header(__type) { \
    static int i = 0; \
    static int _n = 0; \
    static __type* _h; \
    if (head != NULL && size != -1) { \
        _h = head; \
        _n = size; \
        i = 0; \
    } else if (_h != NULL && i < _n) { \
        return &(_h[i++]); \
    } \
    return NULL; \
}

#endif	/* LIB__H */

